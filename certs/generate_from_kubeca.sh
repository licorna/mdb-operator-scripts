#!/usr/bin/env bash


NAMESPACE="${1}"
MDB="test-tls-base-rs-require-ssl"
destination_dir="~/workspace/go/src/github.com/10gen/ops-manager-kubernetes/docker/mongodb-enterprise-tests/tests/tls/fixtures"
ca_context="lm-testing-cluster.mongokubernetes.com"

MDB="my-replica-set"

generate_cert_for_mms_server () {
    host="${1}"
    cat <<EOF | cfssl genkey - | cfssljson -bare "${host}"
{
  "hosts": [
    "${host}"
  ],
  "CN": "${host}",
  "names": [
  {
    "C": "US",
    "ST": "New York",
    "L": "New York",
    "O": "mongodb",
    "OU": "cloud"
  }
  ],
  "key": {
    "algo": "rsa",
    "size": 2048
  }
}
EOF


    cat <<EOF | kubectl apply -f -
apiVersion: certificates.k8s.io/v1beta1
kind: CertificateSigningRequest
metadata:
  name: ${host}.${NAMESPACE}
spec:
  groups:
  - system:authenticated
  request: $(cat ${host}.csr | base64 | tr -d '\n')
  usages:
  - digital signature
  - key encipherment
  - server auth
  - client auth
EOF

    kubectl describe "csr/${host}.${NAMESPACE}"
    kubectl certificate approve ${host}.${NAMESPACE}

    kubectl get csr/${host}.${NAMESPACE} -o jsonpath='{.status.certificate}' \
        | base64 --decode > ${host}.crt

    cat ${host}.crt ${host}-key.pem > ${host}-full.pem
}

generate_cert_for_client () {
    client="${1}"
    cat <<EOF | cfssl genkey - | cfssljson -bare "${client}"
{
  "hosts": [
    "${MDB}-svc.testing03.svc.cluster.local",
    "${client}.${MDB}-svc.testing03.svc.cluster.local",
    "${MDB}.${MDB}-svc.testing03.svc.cluster.local",
    "${client}"
  ],
  "CN": "${client}",
  "names": [
  {
    "C": "US",
    "ST": "New York",
    "L": "New York",
    "O": "cloud",
    "OU": "${MDB}"
  }
  ],
  "key": {
    "algo": "rsa",
    "size": 2048
  }
}
EOF


    ## -> server.csr and server-key.pem

    cat <<EOF | kubectl apply -f -
apiVersion: certificates.k8s.io/v1beta1
kind: CertificateSigningRequest
metadata:
  name: ${client}.${NAMESPACE}
spec:
  groups:
  - system:authenticated
  request: $(base64 < "${client}.csr" | tr -d '\n')
  usages:
  - digital signature
  - key encipherment
  - server auth
  - client auth
EOF

    kubectl describe "csr/${client}.${NAMESPACE}"
    kubectl certificate approve "${client}.${NAMESPACE}"

    kubectl get "csr/${client}.${NAMESPACE}" -o jsonpath='{.status.certificate}' \
        | base64 --decode > "${client}.crt"

    cat "${client}.crt" "${client}-key.pem" > "${client}-full.pem"
}

copy_into_pods() {
    file="${1}"
    members=$(kubectl -n ${NAMESPACE} get mdb/${MDB} -o jsonpath='{.status.members}')
    members=$((members-1))

    for i in $(seq $members -1 0); do
        kubectl cp $file "${NAMESPACE}/${MDB}-$i:/mongodb-automation/"
    done
}

# clients="mms-automation-agent
# mms-monitoring-agent
# mms-backup-agent
# mms-user"
# for cert in $clients; do
#     generate_cert_for_client "${cert}"
#     # copy_into_pods "${cert}-full.pem"
# done

mkdir -p "${MDB}"
# current_context=$(kubectl config current-context)

# kubectl config use-context "${ca_context}"
echo "Currently on $(kubectl config current-context)"
pushd "${MDB}"
for i in $(seq 0 4); do
    generate_cert_for_client "${MDB}-${i}"
done

mv ${MDB}-?-full.pem ${destination_dir}
popd

# kubectl config use-context "${current_context}" --namespace="${NAMESPACE}"
echo "Currently on $(kubectl config current-context)"
