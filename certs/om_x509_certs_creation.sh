#!/usr/bin/env bash

user="ubuntu"

hosts="ec2-3-82-109-30.compute-1.amazonaws.com"

if [ -f index.txt ]; then
    rm index*
fi
touch index.txt
echo '01' > serial.txt


cp openssl-mongodb-rs-remote.cnf openssl-mongodb-rs-remote.cnf_backup
cp openssl-monitoring-agent.cnf openssl-monitoring-agent.cnf_backup
cp openssl-automation-agent.cnf openssl-automation-agent.cnf_backup
cp openssl-mms-server.cnf openssl-mms-server.cnf_backup

i=1
for host in ${hosts}; do
    echo "DNS.${i}  = ${host}" >> openssl-mongodb-rs-remote.cnf
    echo "DNS.${i}  = ${host}" >> openssl-monitoring-agent.cnf
    echo "DNS.${i}  = ${host}" >> openssl-automation-agent.cnf
    echo "DNS.${i}  = ${host}" >> openssl-mms-server.cnf

    i=$((i + 1))
done

# Certificates:
# OU=mongodb
# 1. CA certificate (openssl-ca.cnf)
# 2. Cluster File & PemKey File (openssl-mongodb-rs-remote.cnf )
#   -> Should this be the same?
#   -> Cluster File needs server authentication

# OU=cloud
# 3. Automation Agent client cert (openssl-automation-agent.cnf)
# 4. Monitoring Agent client cert (openssl-monitoring-agent.cnf)

# First. Create the CA

openssl req -x509 -config openssl-ca.cnf -newkey rsa:4096 -sha256 -nodes -out cacert.pem -outform PEM \
        -subj "/C=US/ST=New York/L=New York/O=MongoDB/OU=mongodb/CN=www.mongodb.com"

# MMS HTTPS Server
openssl req -config openssl-mms-server.cnf -newkey rsa:2048 -sha256 -nodes -out mms-server.csr -outform PEM \
        -subj "/C=US/ST=New York/L=New York/O=MongoDB/OU=mongodb/CN=ec2-3-82-109-30.compute-1.amazonaws.com"
openssl ca -config openssl-ca.cnf -policy signing_policy -extensions signing_req -out mms-server.pem -infiles mms-server.csr
cat mms-server.pem mms-server.key > mms-server-full.pem


# Cluster File
openssl req -config openssl-mongodb-rs-remote.cnf -newkey rsa:2048 -sha256 -nodes -out mongodb-rs-remote.csr -outform PEM \
        -subj "/C=US/ST=New York/L=New York/O=MongoDB/OU=mongodb/CN=www.mongodb.com"
openssl ca -config openssl-ca.cnf -policy signing_policy -extensions signing_req -out mongodb-rs-remote.pem -infiles mongodb-rs-remote.csr
cat mongodb-rs-remote.pem mongodb-rs-remote.key > mongodb-rs-remote-full.pem
echo "Created clusterfile as mongodb-rs-remote-full.pem"

openssl req -config openssl-automation-agent.cnf -newkey rsa:2048 -sha256 -nodes -out mms-automation-agent.csr -outform PEM \
        -subj "/C=US/ST=New York/L=New York/O=MongoDB/OU=cloud/CN=mms-automation-agent"
openssl ca -config openssl-ca.cnf -policy signing_policy -extensions signing_req -out mms-automation-agent.pem -infiles mms-automation-agent.csr
cat mms-automation-agent.pem mms-automation-agent.key > mms-automation-agent-full.pem
echo "Created Automation certificate as mms-automation-agent-full.pem"

openssl req -config openssl-monitoring-agent.cnf -newkey rsa:2048 -sha256 -nodes -out mms-monitoring-agent.csr -outform PEM \
        -subj "/C=US/ST=New York/L=New York/O=MongoDB/OU=cloud/CN=mms-monitoring-agent"
openssl ca -config openssl-ca.cnf -policy signing_policy -extensions signing_req -out mms-monitoring-agent.pem -infiles mms-monitoring-agent.csr
cat mms-monitoring-agent.pem mms-monitoring-agent.key > mms-monitoring-agent-full.pem
echo "Created Monitoring certificate as mms-monitoring-agent-full.pem"

echo "Copying certificates to EC2 hosts"
certificates="cacert.pem mongodb-rs-remote-full.pem mms-automation-agent-full.pem mms-monitoring-agent-full.pem"
for host in ${hosts}; do
    for cert in ${certificates}; do
        echo "noop"
        echo scp "${cert}" "${user}@${host}:"
    done
done

# rm openssl-mongodb-rs-remote.cnf openssl-mongodb-rs-remote.cnf_backup
# rm openssl-monitoring-agent.cnf openssl-monitoring-agent.cnf_backup
# rm openssl-automation-agent.cnf openssl-automation-agent.cnf_backup

mv openssl-mongodb-rs-remote.cnf_backup openssl-mongodb-rs-remote.cnf
mv openssl-monitoring-agent.cnf_backup openssl-monitoring-agent.cnf
mv openssl-automation-agent.cnf_backup openssl-automation-agent.cnf
mv openssl-mms-server.cnf_backup openssl-mms-server.cnf
