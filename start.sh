#!/usr/bin/env bash

# More environment variables we need
## 1. avoid insecure certs in urllib
## 2. avoid deprecated yaml.load warnings (used by kubernetes module)
export PYTHONWARNINGS="ignore"

# TODO: Make the venv if it does not exist
# shellcheck source=/dev/null
virtual_envs_dir="${HOME}/.virtualenv"
if [ ! -d "${virtual_envs_dir}" ]; then
    virtual_envs_dir="${HOME}/.virtualenvs"
fi

if [ ! -d "${virtual_envs_dir=}" ]; then
    echo "Virtual envs directory (~/.virtualenv) should exist!"
    exit 1
fi

mci="${virtual_envs_dir}/mms/bin/mci"

# shellcheck source=/dev/null
source "${virtual_envs_dir}/operator-tests/bin/activate"


# TODO: this should be set depending on the cluster type reported by kubectl
MANAGED_SECURITY_CONTEXT="false"
if kubectl cluster-info | grep "running at" | grep -q openshift; then
    MANAGED_SECURITY_CONTEXT="true"
fi
# need to be exported because they need to exist in function execution context.
export MANAGED_SECURITY_CONTEXT

quay_login () {
    if [ -z "${QUAY_SECURE}" ]; then
        echo "QUAY_SECURE is not defined"
        return
    fi
    docker login -u="licorna" -p="${QUAY_SECURE}" quay.io
    # docker login -u="licorna+local_builder" -p="${QUAY_SECURE}" quay.io
}

ecr_login() {
    eval "$(aws ecr get-login --no-include-email --region us-east-1)"
}

mci_spawn() {
    host_name="on-prem-4.0.11-vanilla"
    if ! "${mci}" distros | grep -q "${host_name}"; then
        echo "${host_name} not available in MCI, maybe it was removed?"

    else
        "${mci}" spawn "${host_name}"
        echo "Waiting for new MCI instance"
        while ! "${mci}" list | grep -q "running"; do printf . && sleep 1; done
        echo

        echo "New MCI instance reached 'running' state."
    fi
}

configure_ops_manager () {
    if [ -n "${1}" ]; then
        echo "Configuring ${1}"
        export OPS_MANAGER_HOST="${1}"
    else
        echo "Spawning a new Ops Manager"
        mci_spawn

        if ! mci list | grep -q "ubuntu"; then
            echo "could not start ops manager with mci"
            return
        fi
        host=$("${mci}" list | grep running | awk '{ print $3}' | cut -d "@" -f 2)
        export OPS_MANAGER_HOST="http://${host}:8080"
    fi


    echo "Waiting for Ops Manager to start responding on HTTP"
    # still ops manager has not woken up
    while ! curl -s "${OPS_MANAGER_HOST}"; do printf . && sleep 3; done
    echo

    echo "Configuring Ops Manager"
    api_key=$(curl -s -X POST "${OPS_MANAGER_HOST}/api/public/v1.0/unauth/users?whitelist=174.129.90.133&whitelist=3.87.243.190&whitelist=3.83.51.192&whitelist=54.152.105.255&whitelist=18.212.181.241" \
                   --data '{"username": "admin", "password": "admin12345%", "firstName": "Automated", "lastName": "Automated"}' \
                   -H "Content-Type: application/json" | jq '.apiKey' -M -r)
    echo "This is our new api key: ${api_key}"

    {
        echo "export OM_HOST=${OPS_MANAGER_HOST}"
        echo "export OM_USER=admin"
        echo "export OM_PASSWORD=admin12345%"
        echo "export OM_API_KEY=${api_key}"
    } >  ~/.operator-dev/om

    echo "Remember to 'source' ~/.operator-dev/om before configuring the operator"
}

get_organization () {
    orgid="${1}"
    source ~/.operator-dev/om

    echo $OM_HOST
    echo $OM_USER
    echo $OM_API_KEY

    curl "${OM_HOST}/api/atlas/v1.0/orgs" --include --user "${OM_USER}:${OM_API_KEY}" --digest \
         --header 'Accept: application/json'
}

configure_operator () {
    if [ -f ~/.operator-dev/om ]; then
        # shellcheck source=/dev/null
        source ~/.operator-dev/om

        kubectl -n $NAMESPACE delete configmap/my-project &> /dev/null || true
        kubectl -n $NAMESPACE delete secret/my-credentials &> /dev/null || true

        # Configuring project

        project_opts=(
            "--from-literal=projectName=${NAMESPACE}"
            "--from-literal=baseUrl=${OM_HOST}"
            "--from-literal=credentials=my-credentials"
            "--from-literal=authenticationMode="
        )

        if [ -v OM_ORGID ]; then
            project_opts+=("--from-literal=orgId=${OM_ORGID}")
        fi

        kubectl --namespace "${NAMESPACE}" create configmap my-project "${project_opts[@]}"

        # Configure the Kubernetes credentials for Ops Manager
        kubectl --namespace "${NAMESPACE}" create secret generic my-credentials \
                --from-literal=user="${OM_USER}" --from-literal=publicApiKey="${OM_API_KEY}"

    fi
}

print_automation_config() {
    group_name="$1"

    group_id=$(curl -s "${OM_HOST}/api/public/v1.0/groups/byName/${group_name}" \
                    --header "Accept: application/json" \
                    --user "${OM_USER}:${OM_API_KEY}" \
                    --digest | jq .id -r)

    curl -s "${OM_HOST}/api/public/v1.0/groups/${group_id}/automationConfig" \
         --header "Accept: application/json" \
         --user "${OM_USER}:${OM_API_KEY}" \
         --digest | jq .
}

install_operator () {
    db_version="${1}"
    outdir="helm_out"
    mkdir -p "${outdir}"

    helm template \
         --set namespace="${NAMESPACE}" \
         --set operator.version="latest" \
         --set database.version="${tag}" \
         --set needsCAInfrastructure="true" \
         --set registry.repository="quay.io/licorna" \
         --set operator.name="mdb-operator" \
         --set database.name="mdb-database" \
         --set database.version="${db_version}" \
         --set registry.pullPolicy="Always" \
         --set operator.env="invalid" \
         --set managedSecurityContext="${MANAGED_SECURITY_CONTEXT:true}" \
         -f scripts/evergreen/deployments/values-test.yaml public/helm_chart --output-dir "${outdir}"

    template_dir="${outdir}/mongodb-enterprise-operator/templates"

    echo "Installing Operator"
    for i in serviceaccount roles operator; do
        kubectl -n "${NAMESPACE}" apply -f "${template_dir}/${i}.yaml"
    done

    echo "Restarting operator"
    kubectl -n "${NAMESPACE}" delete "$(kubectl -n ${NAMESPACE} get pods -l app=mdb-operator -o name)"
}

get_automation_config () {
    if [ -z "${OM_USER}" ]; then
        source ~/.operator-dev/om
    fi

    group_id=$(curl -u "${OM_USER}:${OM_API_KEY}" "https://cloud-qa.mongodb.com/api/public/v1.0/groups/byName/${NAMESPACE}" --digest -sS | jq -r .id)

    get_automation_config_by_group_id "$group_id"
}

get_automation_config_by_group_id() {
    if [ -z "${OM_USER}" ]; then
        source ~/.operator-dev/om
    fi

    group_id="${1}"

    curl -u "${OM_USER}:${OM_API_KEY}" "https://cloud-qa.mongodb.com/api/public/v1.0/groups/${group_id}/automationConfig" --digest -sS
}

read_automation_config () {
    config=$(get_automation_config)
    f="$(mktemp).json"

    echo "$config" | jq . >> "$f"
    $EDITOR -n "$f"
}

# reinstall_10_times() {
#     for i in $(seq 10); do
#         echo "Installing test_$1"
#         install_operator "test_$i"
#         sleep 10

#         phase=$(kubectl get mdb/my-replica-set -o jsonpath='{.status.phase}')
#         echo "Waiting for mdb resource to get to running state (now is: $phase)"
#         while [ $(kubectl get mdb/my-replica-set -o jsonpath='{.status.phase}') != "Running" ]; do printf "."; sleep 1; done
#     done

#     # echo "namespace is ${NAMESPACE}"
#     kubectl -n "${NAMESPACE}" delete $(kubectl -n "${NAMESPACE}" get pods -l app=mdb-operator -o name)
# }

install_operator_rhel_prebuilt () {
    tag="${1}"
    outdir="helm_out"
    mkdir -p "${outdir}"

    docker login -u unused scan.connect.redhat.com -p "${RH_OPERATOR_KEY}"
    # operator_image="registry.connect.redhat.com/mongodb/enterprise-operator"
    operator_image="scan.connect.redhat.com/ospid-5558a531-617e-46d7-9320-e84d3458768a/partner-build-service:${tag}"
    docker pull "${operator_image}"

    docker login -u unused scan.connect.redhat.com -p "${RH_DATABASE_KEY}"
    # database_image="registry.connect.redhat.com/mongodb/enterprise-database"
    database_image="scan.connect.redhat.com/ospid-239de277-d8bb-44b4-8593-73753752317f/partner-build-service:${tag}"
    docker pull "${database_image}"

    docker tag "${operator_image}" "quay.io/licorna/mdb-operator:${tag}"
    docker tag "${database_image}" "quay.io/licorna/mdb-database:${tag}"

    docker tag "${operator_image}" "quay.io/licorna/mdb-operator:latest"
    docker tag "${database_image}" "quay.io/licorna/mdb-database:latest"

    docker push "quay.io/licorna/mdb-database:${tag}"
    docker push "quay.io/licorna/mdb-operator:${tag}"

    docker push "quay.io/licorna/mdb-database:latest"
    docker push "quay.io/licorna/mdb-operator:latest"

    helm template \
         --set namespace="${NAMESPACE}" \
         --set operator.version="${tag}" \
         --set needsCAInfrastructure="true" \
         --set registry.repository="quay.io/licorna" \
         --set operator.name="mdb-operator" \
         --set database.name="mdb-database" \
         --set registry.pullPolicy="Always" \
         --set operator.env="dev" \
         --set managedSecurityContext="${MANAGED_SECURITY_CONTEXT:true}" \
         -f scripts/evergreen/deployments/values-test.yaml public/helm_chart --output-dir "${outdir}"

    template_dir="${outdir}/mongodb-enterprise-operator/templates"

    echo "Installing Operator"
    for i in serviceaccount roles operator; do
        kubectl -n "${NAMESPACE}" apply -f "${template_dir}/${i}.yaml"
    done

}

# Install the operator from latest RHEL
install_operator_rhel_images () {
    docker pull registry.connect.redhat.com/mongodb/enterprise-database
    docker pull registry.connect.redhat.com/mongodb/enterprise-operator

    docker tag registry.connect.redhat.com/mongodb/enterprise-database quay.io/licorna/mdb-database
    docker tag registry.connect.redhat.com/mongodb/enterprise-operator quay.io/licorna/mdb-operator

    docker push quay.io/licorna/mdb-database
    docker push quay.io/licorna/mdb-operator

    # build_images_operator

    install_operator
}

build_images_database () {
    echo "Building database image"
    make -C docker/mongodb-enterprise-database push IMAGE_DIR=licorna IMAGE=mdb-database AWS_IMAGE_REPO=quay.io

    # for i in $(seq 10); do
    #     make -C docker/mongodb-enterprise-database push IMAGE_DIR=licorna IMAGE=mdb-database AWS_IMAGE_REPO=quay.io IMAGE_VERSION="test_$i"
    # done

}

build_images_operator () {
    echo "Building operator image"
    make -C docker/mongodb-enterprise-operator push IMAGE_DIR=licorna IMAGE=mdb-operator AWS_IMAGE_REPO=quay.io
}

generate_certs() {
    echo "cleaning"
    rm -- *.pem *.crt *.csr *.srl *.key
    echo "killing running mongos"

    echo "generate CA"
    openssl genrsa -out root_ca.key 2048
    openssl req -x509 -new -nodes -key root_ca.key -sha256 -days 1024 -out root_ca.pem -subj "/C=IE/ST=Dublin/L=Dublin/O=MongoDB/OU=Cloud/CN=server.com"


    echo "generating csr"
    for i in $(seq 0 1 2); do
        openssl req -nodes -newkey rsa:2048 -keyout "server${i}.key" -out "server${i}.csr" \
                -subj "/C=IE/ST=Dublin/L=Dublin/O=MongoDB/OU=Cloud/CN=localhost"
    done

    echo "generating self signed certs"
    for i in $(seq 0 1 2); do
        openssl x509 -req -in "server${i}.csr" -CA root_ca.pem -CAkey root_ca.key -CAcreateserial -signkey "server${i}.key" -out "server${i}.crt" -days 365
        cat "server${i}.crt" "server${i}.key" > "server${i}.pem"
    done
}

start_secure_rs() {
    source ~/workspace/10gen/mdb-operator-scripts/start.sh

    start_secure_rs0
}

start_secure_rs0() {
    # shellcheck disable=SC2009
    ps aux | grep mongod | grep "dbpath db" | grep "port 3700" | awk '{print $2}' | xargs kill &> /dev/null
    rm -rf db{0,1,2}/
    sleep 2

    echo "Starting mongods with SSL options"
    mkdir db{0,1,2}
    for i in $(seq 0 1 2); do
        #
        # --clusterAuthMode x509 \
        mongod --dbpath "db${i}/" --logpath "db${i}/mongod.log" --port "3700${i}" --replSet "secure0"\
               --sslMode requireSSL \
               --sslPEMKeyFile mongodb-full.pem \
               --sslClusterFile mongodb-full.pem \
               --clusterAuthMode x509 \
               --sslCAFile cacert.pem \
               --fork
    done

    rsconfig='rs.initiate( {_id : "secure0", members: [ { _id: 0, host: "localhost:37000" }, { _id: 1, host: "localhost:37001" }, { _id: 2, host: "localhost:37002" }] })'
    mongo --host "localhost" --port 37000 \
          --eval "${rsconfig}" \
          --ssl --sslCAFile "cacert.pem" \
          --sslPEMKeyFile mms-monitoring-agent-full.pem

    echo "wait until mongodb replica set has finished configuring"
    sleep 5
    mongo --host "localhost" --port 37001 \
          --eval "rs.slaveOk()" \
          --ssl --sslCAFile "cacert.pem"
}

oplogs () {
    while true; do
        source ~/.operator-dev/contexts/openshift # just in case NAMESPACE changed
        while ! kubectl -n "${NAMESPACE}" get pods -l app=mdb-operator -o jsonpath='{.items[*].status.phase}' | grep -q "Running"; do sleep 1; done
        kubectl -n "${NAMESPACE}" logs "$(kubectl -n ${NAMESPACE} get pods -l app=mdb-operator -o name)" -f
        sleep 2
    done
}

inspect_json () {
    pod="${1}"
    file="${2}"
    f=$(mktemp) && kubectl exec -it "$pod" cat "$file" | jq . > $f && emacsclient $f
}

certs_approve () {
    kubectl certificate approve $(kubectl get csr -o name | grep $NAMESPACE)
}

certs_copy () {
    kubectl cp "${NAMESPACE}/my-secure-rs-0:/mongodb-automation/server.pem" server0.pem
    kubectl cp "${NAMESPACE}/my-secure-rs-1:/mongodb-automation/server.pem" server1.pem
    kubectl cp "${NAMESPACE}/my-secure-rs-2:/mongodb-automation/server.pem" server2.pem
    kubectl cp "${NAMESPACE}/my-secure-rs-2:/var/run/secrets/kubernetes.io/serviceaccount/..data/ca.crt" ca.crt
}

clean () {
    echo "Working on ${NAMESPACE}"

    echo "Removing certificates"
    kubectl delete $(kubectl get csr -o name | grep ".${NAMESPACE}")

    echo "Removing certificate secrets on this namespace"
    for obj in $(kubectl -n ${NAMESPACE} get secrets -l "mongodb/secure=certs" -o name); do
        kubectl -n ${NAMESPACE} delete "$obj"
    done

    echo "Removing MongoDB resources"
    kubectl -n ${NAMESPACE} delete mdb --all || true
    echo "This might go too fast and the operator could be removed before all the mdb resources are removed."
}

start_regular () {
    echo "Building images"
    build_images_database
    build_images_operator

    echo "Installing operator"
    install_operator
}

start_rhel () {
    echo "Installing operator in RHEL"

    tag="${RHEL_OPERATOR_VERSION:-latest}"
    install_operator_rhel_prebuilt "$tag"
}

# restart_replica_set () {
#     sts_name="my-replica-set"

#     for i in $(seq 10); do
#         while [ "$(kubectl get mdb/${sts_name} -o jsonpath='{.status.phase}')" != "Running" ]; do sleep 3; printf "."; done

#         echo "installing new operator version"
#         install_operator "test_${i}"

#         # wait until this change has propagated all the way to the statefulset + the operator,
#         # so the MDB resource moves out of running state.
#         sleep 10
#     done
# }

restart_mdb_ss() {
    sts="${1}"
    kubectl patch "statefulset/${sts}" -p \
            "{\"spec\":{\"template\":{\"metadata\":{\"annotations\":{\"dummy-date\":\"`date +'%s'`\"}}}}}"
}

remove_readiness_probe() {
    pod="${1}"

    kubectl exec "${pod}" -- mv /mongodb-automation/files/readinessprobe /mongodb-automation/files/readinessprobe.bak
    kubectl exec "${pod}" -- ln -s /bin/true /mongodb-automation/files/readinessprobe
}

new_namespace() {
    source ~/.operator-dev/contexts/openshift

    IFS='-' read -ra namespace_parts <<< "$NAMESPACE"
    new_namespace="${namespace_parts[0]}-$((namespace_parts[1] + 1))"

    sed -i "s/NAMESPACE=.*/NAMESPACE=${new_namespace}/g" ~/.operator-dev/contexts/openshift
    sed -i "s/PROJECT_NAMESPACE=.*/PROJECT_NAMESPACE=${new_namespace}/g" ~/.operator-dev/contexts/openshift
    source ~/.operator-dev/contexts/openshift

    oc new-project "$new_namespace"
    rebuild
    configure_operator

    echo "Opening Openshift UI"
    xdg-open "https://console.openshift-cluster.mongokubernetes.com/k8s/ns/${new_namespace}/pods"
}



rebuild () {
    rebuild_opts="${1}"

    pushd "$(git rev-parse --show-toplevel)" || exit

    if [[ $rebuild_opts = "rhel" ]]; then #
        install_operator_rhel_images
    else
        start_regular
    fi

    popd || exit
}
